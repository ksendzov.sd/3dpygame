import pygame
from math import *
import time
import random
from threading import Thread

White = (255, 255, 255)
Grey = (100, 100, 100)
White_Grey = (200, 200, 200)
Red = (255, 0, 0)
Green = (0, 255, 0)
Blue = (0, 0, 255)
White_Blue = (100, 100, 255)
Ground = (162,101,62)

points = 0
pollygons = 1
blocks = 2


class Main:

    def __init__(self):
        self.jumping = False
        self.jump = 0
        self.duck = False
        self.running = True
        self.tmtosort = 0
        self.all_obg = [[],[],[]]
        self.xcm = 0
        self.ycm = 0
        self.zcm = 0
        self.ypl = 30
        self.movevec = [0, 0, 0]
        self.rotationx = 0
        self.rotationy = 0
        self.rotationz = 0
        self.fov = 600
        (self.width, self.height) = (1000, 600)
        self.screen = pygame.display.set_mode((self.width, self.height))
        self.background_colour = (0, 0, 30)

    def rotx(self, x1, y1, z1, a):
        y = y1 * cos(a) + z1 * sin(a)
        z = y1 * -1 * sin(a) + z1 * cos(a)
        return x1, y, z

    def roty(self, x1, y1, z1, a):
        x = x1 * cos(a) + z1 * sin(a)
        z = x1 * -1 * sin(a) + z1 * cos(a)
        return x, y1, z

    def rotz(self, x1, y1, z1, a):
        x = x1 * cos(a) + y1 * -1 * sin(a)
        y = x1 * sin(a) + y1 * cos(a)
        return x, y, z1

    def rotxyz(self,x1,y1,z1,a,b,c):
        x = x1*cos(b)*cos(c) + y1*(-1*cos(b)*sin(c)) + z1 * sin(b)
        y = x1*(sin(a)*sin(b)*cos(c)+cos(a)*sin(c)) +y1*(-1*sin(a)*sin(b)*sin(c)+cos(a)*cos(c)) + z1*(-1*sin(a)*cos(b))
        z = x1*(-1*cos(a)*sin(b)*cos(c)+sin(a)*sin(c)) + y1*(cos(a)*sin(b)*sin(c)+sin(a)*cos(c)) + z1*(cos(a)*cos(b))
        return x,y,z

    def multmat(self,x,y,z):
        x, y, z = x - self.xcm, y - self.ycm, z - self.zcm
        x, y, z = self.rotxyz(-x, -y, z, -self.rotationy / 57.6,self.rotationx / 57.6,self.rotationz / 57.6)
        if not z:
            z += 0.01
        return [x * self.fov / z, y * self.fov / z, z]


    def drawcrl(self, xob, yob, zob, rad, clr=White):
        x, y, z = self.multmat(xob,yob,zob)
        if z > 10:
            pygame.draw.circle(
                self.screen, (20, 20, 20),
                (x + (self.width / 2),
                 y + (self.height / 2)),
                (rad + 0.1) * self.fov / z)
            pygame.draw.circle(
                self.screen, clr, (x + (self.width / 2),
                                   y + (self.height / 2)),
                rad * self.fov / z)

    def drawpol(self, cordobj, clr=Grey):
        cordobj2 = []
        dr = True
        for i in range(len(cordobj)):
            cordobj2.append(self.multmat(cordobj[i][0],cordobj[i][1],cordobj[i][2]))
            cordobj2[i][0] +=(self.width / 2)
            cordobj2[i][1] +=(self.height / 2)
        y = range(len(cordobj2))
        for i in y:
            #if cordobj2[i][2] < -10 or cordobj2[i][1] > (self.height)+240 or cordobj2[i][1] < -240 or cordobj2[i][0] > (self.width)+240 or cordobj2[i][0] < -240:
            #    if cordobj2[i][0] > (self.width / 2):
            #        cordobj2[i][0] = (self.width)
            #    elif cordobj2[i][0] < (self.width / 2):
            #        cordobj2[i][0] = 0
            #    if cordobj2[i][1] > (self.height / 2):
            #        cordobj2[i][1] = (self.height)
            #    elif cordobj2[i][1] < (self.height / 2):
            #        cordobj2[i][1] = 0
            if cordobj2[i][2] < -30 or cordobj2[i][1] > (self.height) + 240 or cordobj2[i][1] < -240 or cordobj2[i][0] > (self.width) + 240 or cordobj2[i][0] < -240:
                dr = False
            cordobj2[i].pop(2)
        if dr:
            pygame.draw.polygon(self.screen, clr, cordobj2)

    def drawcube(self, x,y,z,clr=Grey):
        cord2 = []
        cord2.append((x, y, z))
        cord2.append((x, y+40, z))
        cord2.append((x, y+40, z+40))
        cord2.append((x+40, y+40, z+40))
        cord2.append((x+40, y, z+40))
        cord2.append((x+40, y, z))
        self.drawpol(cord2,clr)

    def drawblc(self, obj):
        for i in range(1,len(obj)-1):
            if obj[i][0][0] != 0:
                self.drawcrl(i * 40, -90, 0, 20, Green)
            if obj[i][0][-1] != 0:
                self.drawcrl(i * 40, -90, len(obj[i][0])*40-40, 20, Green)
            for l in range(1,len(obj[i][0])-1):
                if obj[i][0][l] != 0:
                    self.drawcrl(i*40,-90,l*40, 20,Green)

            if obj[i][-1][0] != 0:
                self.drawcrl(i * 40, len(obj[i])*40-130, 0, 20, Green)
            if obj[i][-1][-1] != 0:
                self.drawcrl(i * 40, len(obj[i])*40-130, len(obj[i][0])*40-40, 20, Green)
            for l in range(1,len(obj[i][0])-1):
                if obj[i][-1][l] != 0:
                    self.drawcrl(i*40,len(obj[i])*40-130,l*40, 20,Green)

            for j in range(1,len(obj[i])-1):
                if obj[i][j][0] != 0:
                    self.drawcrl(i * 40, j * 40 - 90, 0, 20, Green)
                if obj[i][j][-1] != 0:
                    self.drawcrl(i * 40, j * 40 - 90, len(obj[i][j])*40-40, 20, Green)
                for l in range(1,len(obj[i][j])-1):
                    if obj[i][j][l] != 0 and (obj[i+1][j][l] != 0 and obj[i][j+1][l] != 0 and obj[i][j][l+1] != 0 and obj[i-1][j][l] != 0 and obj[i][j-1][l] != 0 and obj[i][j][l-1] != 0):
                        self.drawcrl(i*40,j*40-90,l*40, 20,Blue)

    def drawline(self, x1,y1,z1,x2,y2,z2, clr=Grey):
        x1,y1,z1 = self.multmat(x1,y1,z1)
        x2,y2,z2 = self.multmat(x2,y2,z2)
        if z1 > -5 and z2 > -5:
            pygame.draw.line(self.screen,clr, (x1+(self.width / 2),y1+(self.height / 2)), (x2+(self.width / 2),y2+(self.height / 2)))

    def drawsky(self):
        pygame.draw.rect(self.screen, Ground, (0,0,self.width,self.height))
        if self.rotationy > 180:
            rotsk = self.rotationy - 360
        else:
            rotsk = self.rotationy
        y = (rotsk)*11+300
        pygame.draw.rect(self.screen, White_Blue, (0,0,self.width,y))

    def sortxycrl(self, obj):
        global xcm
        global ycm
        global zcm
        for j in range(len(obj)):
            for i in range(len(obj) - 1):
                x = obj[i][0]
                y = obj[i][1]
                z = obj[i][2]
                x1 = obj[i + 1][0]
                y1 = obj[i + 1][1]
                z1 = obj[i + 1][2]
                if sqrt(
                    (sqrt(((x - self.xcm) ** 2) +
                               ((z - self.zcm) ** 2))) +
                    ((y - self.zcm) ** 2)) < sqrt(
                    (sqrt(((x1 - self.xcm) ** 2) +
                               ((z1 - self.zcm) ** 2))) +
                        ((y1 - self.zcm) ** 2)):
                    a = obj[i]
                    obj[i] = obj[i + 1]
                    obj[i + 1] = a
        return obj

    def sortxypol_bad(self, obj):
        global xcm
        global ycm
        global zcm
        for j in range(len(obj)):
            for i in range(len(obj) - 1):
                x = obj[i][0]
                y = obj[i][1]
                z = obj[i][2]
                x1 = obj[i + 1][0]
                y1 = obj[i + 1][1]
                z1 = obj[i + 1][2]
                if sqrt(
                    (sqrt(((x - self.xcm) ** 2) +
                               ((z - self.zcm) ** 2))) +
                    ((y - self.zcm) ** 2)) < sqrt(
                    (sqrt(((x1 - self.xcm) ** 2) +
                               ((z1 - self.zcm) ** 2))) +
                        ((y1 - self.zcm) ** 2)):
                    a = obj[i]
                    obj[i] = obj[i + 1]
                    obj[i + 1] = a
        return obj

    def render(self):
        while self.running:
            #self.screen.fill(self.background_colour)
            self.rotationx = self.rotationx % 360
            self.rotationy = self.rotationy % 360
            self.rotationz = self.rotationz % 360
            self.drawsky()
            if self.tmtosort > 100:
                self.all_obg[points] = self.sortxycrl(self.all_obg[points])
                #self.all_obg[pollygons] = self.sortxypol_bad(self.all_obg[pollygons])     <--------
                self.tmtosort = 0
            else:
                self.tmtosort += 1
            for i in range(len(self.all_obg[points])):
                self.drawcrl(*self.all_obg[points][i])
            for i in range(len(self.all_obg[pollygons])):
                if len(self.all_obg[pollygons][i]) == 1:
                    self.all_obg[pollygons][i].append(None)
                self.drawpol(self.all_obg[pollygons][i][0],self.all_obg[pollygons][i][1])

            self.drawblc(self.all_obg[blocks])

            #textsurface = myfont.render(str(self.rotationy), False, (0, 0, 0))
            #self.screen.blit(textsurface, (0, 0))
            pygame.display.update()

    def control(self):
        while self.running:
            pressed = pygame.key.get_pressed()
            for event in pygame.event.get():                                                       # НЕ РАЗБИРАТЬСЯ. ОНО ПРОСТО РАБОТАЕТ
                if event.type == pygame.QUIT:
                    self.running = False


            if pressed[pygame.K_LSHIFT]:
                self.speed = 1.5
            else:
                self.speed = 1

            if pressed[pygame.K_w]:
                self.movevec[0] += sin(self.rotationx / 57.6) / 6000*self.speed
                self.movevec[2] += cos(self.rotationx / 57.6) / 6000*self.speed
            if pressed[pygame.K_s]:
                self.movevec[0] -= sin(self.rotationx / 57.6) / 6000*self.speed
                self.movevec[2] -= cos(self.rotationx / 57.6) / 6000*self.speed
            if pressed[pygame.K_a]:
                self.movevec[0] += cos(self.rotationx / 57.6) / 6000*self.speed
                self.movevec[2] -= sin(self.rotationx / 57.6) / 6000*self.speed
            if pressed[pygame.K_d]:
                self.movevec[0] -= cos(self.rotationx / 57.6) / 6000*self.speed
                self.movevec[2] += sin(self.rotationx / 57.6) / 6000*self.speed
            if pressed[pygame.K_f]:
                self.rotationz += 0.1
            if pressed[pygame.K_g]:
                self.rotationz -= 0.1
            if pressed[pygame.K_q]:
                self.rotationy += 0.1
            if pressed[pygame.K_e]:
                self.rotationy -= 0.1
            self.duck = False
            if pressed[pygame.K_c]:
                self.duck = True
            if pressed[pygame.K_SPACE]:
                if not self.jump:
                    self.jump = True
                    self.yst = self.ypl
            self.xcm += self.movevec[0]
            self.ypl += self.movevec[1]
            self.zcm += self.movevec[2]
            self.movevec[0] /= 1.009 + self.duck * 0.02
            self.movevec[2] /= 1.009 + self.duck * 0.02
            self.xms, self.yms = pygame.mouse.get_pos()
            self.rotationx -= (self.xms-self.width/2)/1500
            self.rotationy -= (self.yms-self.height/2)/1500
            pygame.mouse.set_pos(self.width/2,self.height/2)
            self.ycm = self.ypl - 15*self.duck
            if self.jump:
                self.ypl = self.yst + sin(self.jumping / 57.6) * 100
                self.jumping += 0.006
                self.movevec[0] *= 1.005
                self.movevec[2] *= 1.005
                if self.jumping > 179:
                    self.ypl = self.yst
                    self.jump = False
                    self.jumping = 0

    def loop(self):
        self.control()
        #while self.running:

    def loadMap(self):
        #self.all_obg[points].append([0,-5,0, 5])
        for i in range(10):
            for j in range(10):
                self.all_obg[pollygons].append([[[0,0+i*30,0+j*30],[0,30+i*30,0+j*30],[0,30+i*30,30+j*30],[0,0+i*30,30+j*30],],Blue])
                self.all_obg[pollygons].append([[[300,0+i*30,0+j*30],[300,30+i*30,0+j*30],[300,30+i*30,30+j*30],[300,0+i*30,30+j*30],],Blue])
                self.all_obg[pollygons].append([[[0+i*30,0,0+j*30],[30+i*30,0,0+j*30],[30+i*30,0,30+j*30],[0+i*30,0,30+j*30],],Green])
        #self.all_obg[blocks] = [[[1,1,1,1],[1,1,1,1],[1,1,1,1]],[[1,1,1,1],[1,1,1,1],[1,1,1,1]],[[1,1,1,1],[1,1,1,1],[1,1,1,1]]]
        #self.all_obg[pollygons].append([[[-550,-550,-550],[-550,150,-550],[-550,150,2050],[-550,-550,2050]],White_Grey])
        return 0

    def init(self):
        pygame.display.set_caption('3d')
        self.loadMap()
        return 0

    def main(self):
        self.init()
        self.rendth = Thread(target=self.render)
        self.rendth.start()
        self.loop()
        return 0


if __name__ == "__main__":
    pygame.init()
    myfont = pygame.font.SysFont('Comic Sans MS', 30)
    pygame.mouse.set_visible(False)
    app = Main()
    app.main()

